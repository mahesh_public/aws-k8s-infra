resource "aws_route53_zone" "rt53_zone" {
  name = var.domain_name

  provisioner "local-exec" {
    when    = destroy
    command = "./scripts/delete-route53-records.sh ${self.id}"
  }

  tags = {
    Name = "terraform-k8s-rt53-zone"
  }
}

output "route_53" {
  value = {
    zone_id      = aws_route53_zone.rt53_zone.zone_id
    name_servers = aws_route53_zone.rt53_zone.name_servers
  }
}

